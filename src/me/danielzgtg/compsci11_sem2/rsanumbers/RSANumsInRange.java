package me.danielzgtg.compsci11_sem2.rsanumbers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * A program to determine the quantity of RSA numbers in a range.
 * 
 * @author Daniel Tang
 * @since 5 March 2017
 */
public final class RSANumsInRange {

	/**
	 * A list of prime numbers up to the specified range
	 */
	private static final List<Integer> PRIMES;

	/**
	 * The range this program will support for calculations.
	 */
	private static final int MAX_RANGE = 999;

	/**
	 * The prompt layout for obtaining the lower limit
	 */
	private static final String LOWER_LIMIT_PROMPT = "Enter lower limit of range\n";

	/**
	 * The prompt layout for obtaining the upper limit
	 */
	private static final String UPPER_LIMIT_PROMPT = "Enter upper limit of range\n";

	/**
	 * The output layout for the number of RSA numbers in the range.
	 */
	private static final String OUTPUT_LAYOUT = "The number of RSA numbers between %d and %d is %d\n";

	/**
	 * The error message when an integer was expected, but it could not be understood.
	 */
	private static final String ERR_NOT_INT =
			"Sorry, that input could not be understood as an integer, please try again.";

	/**
	 * The error message when the min and the max contradict their definitions.
	 */
	private static final String ERR_MIN_MAX_WRONG =
			"The lower limit is larger than the upper limit. This does not make sense, please try again.";

	/**
	 * The error message when the minimum (and by extension the range) extends into the negatives--unsupported.
	 */
	private static final String ERR_MIN_NOT_POSITIVE =
			"The lower limit is less than 1. This does not make sense for RSA numbers, please try again.";

	/**
	 * The error message when the maximum (and by extension the range) is past the maximum range supported.
	 */
	private static final String ERR_MAX_OUT_OF_RANGE =
			"Sorry, but the upper limit is larger than this program supports, please try again.";

	static {
		// Generates the list of primes up to the max range
		final List<Integer> tmpList = new ArrayList<>();

		OUTERLOOP:
			// Find all of the primes up to the max range, by means of a loop
			for (int i = 2; i <= MAX_RANGE; i++) {
				// Check if the number is composite of an earlier prime, and so is not a prime
				for (int check : tmpList) {
					if (i % check == 0) continue OUTERLOOP; // Not a prime, skip this one
				}

				tmpList.add(i); // It is, so add it
			}

		// Store the list
		PRIMES = Collections.unmodifiableList(tmpList);
	}

	public static void main(final String[] ignore) {
		int min = 0, max = 0;
		final Scanner scanner = new Scanner(System.in); // Set up scanner
		while (true) { // Keep trying until we have good input
			min = promptInt(LOWER_LIMIT_PROMPT, ERR_NOT_INT, scanner); // Get the min
			max = promptInt(UPPER_LIMIT_PROMPT, ERR_NOT_INT, scanner); // Get the max

			if (min > max) {
				// Complain when the min and max contradict their definitions
				System.out.println(ERR_MIN_MAX_WRONG);
			} else if (min < 1) {
				// Complain if the min is negative--unsupported
				System.out.println(ERR_MIN_NOT_POSITIVE);
			} else if (max > MAX_RANGE) {
				// Complain if the max is out of the supported range
				System.out.println(ERR_MAX_OUT_OF_RANGE);
			} else {
				// We can stop asking for input and move on
				break;
			}
		}

		// Calculate and print out the answer
		System.out.format(OUTPUT_LAYOUT, min, max, countRSANumbersInRange(min, max));
	}

	/**
	 * Prompt the user for an integer
	 * 
	 * @param prompt The message to prompt for the integer with
	 * @param errorMsg The error message when the input could not be understood as an integer
	 * @param scanner The scanner to obtain the input with.
	 * @return The {@code int} obtained from the user
	 */
	private static final int promptInt(final String prompt, final String errorMsg, final Scanner scanner) {
		while (true) { // Loop until good
			System.out.print(prompt); // Prompt the user

			try {
				return scanner.nextInt(); // Try to get an integer and result
			} catch (final InputMismatchException e) {
				System.out.println(errorMsg); // Complain when it is not understood
			} finally {
				scanner.nextLine(); // Get the scanner ready again
			}
		}
	}

	/**
	 * Actually determines the number of RSA numbers in the specified range, inclusive
	 * 
	 * @param min The minimum bound of the range, inclusive
	 * @param max The maximum bound of the range, exclusive
	 * @return The number of RSA numbers counted in the range
	 */
	private static final int countRSANumbersInRange(final int min, final int max) {
		if (min > max) // Min must be less than Max for this to work
			throw new IllegalArgumentException(String.format("Min is greater than max! %d %d", min, max));
		if (min < 1) // Min must be greater than 0 to work
			throw new IllegalArgumentException("Min must be positive!: " + min);
		if (max > MAX_RANGE) // Max must be in the supported range to work
			throw new IllegalArgumentException(String.format("Max is greater than range %d %d", max, MAX_RANGE));

		int result = 0; // Counter variable for result
		for (int i = min; i <= max; i++) { // Check all numbers in the range in a loop
			if (isAnRSANumber(i)) result++; // Make note if the number is an RSA number
		}

		return result;
	}

	/**
	 * Checks whether the specified number is an RSA number.
	 * 
	 * @param check The number to check
	 * @return {@code true} if the number is indeed an RSA number
	 */
	private static final boolean isAnRSANumber(int check) {
		if (check < 1) // This check won't work if the number is not greater than 0
			throw new IllegalArgumentException("Checked number must be positive!: " + check);
		if (check > MAX_RANGE) // This check won't work if the number is outside the range
			throw new IllegalArgumentException(String.format("Checked number is greater than range %d %d", check, MAX_RANGE));
		if (check < 4) // There can't be any numbers with 2 prime factors below 4.
			return false;

		int primes = 0; // Counter variable for primes so far
		for (int i : PRIMES) { // Check with all primes whether the number is a factor
			/* If we are greater than the check,
			 * there won't be any more factors,
			 * so we're done this iteration
			 */
			if (i > check) break;
			// Keep trying to pull out this factor when it is present
			while (check % i == 0) {
				primes++; // Count the factor
				check /= i; // The number being checked is reduced when the factor is pulled out
			}
		}

		return primes == 2; // An RSA number has 2 prime factors
	}
}
